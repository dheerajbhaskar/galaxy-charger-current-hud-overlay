package com.osw.galaxy_charger_current_hud_overlay;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by Dr shobha on 22/8/13.
 */
public class ActivityAbout extends Activity {
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.activity_about);
  }
}
