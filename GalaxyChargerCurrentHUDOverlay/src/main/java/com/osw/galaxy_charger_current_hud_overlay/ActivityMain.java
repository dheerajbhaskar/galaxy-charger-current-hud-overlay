package com.osw.galaxy_charger_current_hud_overlay;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.osw.galaxy_charger_current_hud_overlay.events.DoHUDViewMoveEvent;
import com.osw.galaxy_charger_current_hud_overlay.events.GetNewHandlerReferenceEvent;
import com.osw.galaxy_charger_current_hud_overlay.events.JoystickButtonPressedEvent;
import com.osw.galaxy_charger_current_hud_overlay.events.NewHandlerReference;
import com.osw.galaxy_charger_current_hud_overlay.events.StopServiceEvent;
import com.squareup.otto.Subscribe;
import com.suredigit.inappfeedback.FeedbackDialog;
import com.suredigit.inappfeedback.FeedbackSettings;

public class ActivityMain extends Activity {

  public static final String TAG = "ActivityMain";
  private SharedPreferences sharedPrefs;
  private SharedPreferences.Editor sharedPrefsEditor;
  private Currents mCurrents;
  private TextView mTxtCurrents;
  private FeedbackDialog feedbackDialog;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    Crashlytics.start(this);

    setContentView(R.layout.activity_main);

    //register on otto bus
    BusProvider.getInstance().register(this);

    //get SharedPreferences and its editor
    sharedPrefs = getSharedPreferences(CONST.PREFERENCES_TAG, MODE_PRIVATE);
    sharedPrefsEditor = sharedPrefs.edit();

    //get TextView from XML
    mTxtCurrents = (TextView) findViewById(R.id.txtCurrents);

    //get XML views and add OnTouchListeners to them
    wireTheJoystickButtons();

    //get XML views and add OnClickListeners to them
    wireTheStopHUDAndFeedbackButtons();

    //start the service; start with ApplicationContext since when no Contexts are assoiciated with a serivce, it's stopped
    startService(new Intent(getApplicationContext(), HUDService.class));

    //initializes Android-Feedback Library
    enableInAppFeedback();
  }

  private void wireTheStopHUDAndFeedbackButtons() {
    Button btnStopHUD, btnFeedback;

    //get from XML
    btnFeedback = (Button) findViewById(R.id.btnFeedback);

    // add click listener to btnStopHUD
//        StopHUDBtnListener stopHUDBtnListener = new StopHUDBtnListener();
//        btnStopHUD.setOnClickListener(stopHUDBtnListener);

    //add click listener to btnFeedback
    FeedbackBtnListener feedbackBtnListener = new FeedbackBtnListener();
    btnFeedback.setOnClickListener(feedbackBtnListener);
  }

  /**
   * initializes Android-Feedback Library
   */
  private void enableInAppFeedback() {
    // in-app feedback
    FeedbackSettings feedbackSettings = new FeedbackSettings();
    feedbackSettings.setCancelButtonText("Nope!");
    feedbackSettings.setSendButtonText("Seeeend");
    feedbackSettings.setText("Hey, would you like to give us some feedback so that we can improve your experience?");
    feedbackSettings.setTitle("Jolly Feedback");
    feedbackSettings.setToast("Thank you so much!");
    feedbackSettings.setRadioButtons(true); // Disables radio buttons
    feedbackSettings.setModal(true);
    feedbackSettings.setReplyTitle("Message from the Developer");

    feedbackDialog = new FeedbackDialog(this, CONFIG.ANDROID_FEEDBACK_API_KEY, feedbackSettings);
  }


  /**
   * gets joystick button views from XML and attaches listeners to them
   */
  private void wireTheJoystickButtons() {
    //init buttons
    Button left, right, up, down;

    //fetch buttons from XML
    left = (Button) findViewById(R.id.btnLeft);
    right = (Button) findViewById(R.id.btnRight);
    up = (Button) findViewById(R.id.btnUp);
    down = (Button) findViewById(R.id.btnDown);

    //add OnTouchListeners
    JoystickRepeatListener listener = new JoystickRepeatListener();
    left.setOnTouchListener(listener);
    right.setOnTouchListener(listener);
    up.setOnTouchListener(listener);
    down.setOnTouchListener(listener);
  }

  /**
   * This is the <b>implementation</b> of what particular Joystick events do. The various events that
   * can be received are enums in CONST.JOYSTICK
   * <p/>
   * This method posts a DoHUDViewMoveEvent(DIRECTION) on to the bus, the listening views can decide,
   * how to move and how much to move.
   *
   * @param event one of the events defined in CONST.JOYSTICK
   */
  @Subscribe
  public void doJoystickAction(JoystickButtonPressedEvent event) {
    Crashlytics.log(Log.INFO, TAG, "doJoystickAction: " + event.button);

    switch (event.button) {
      case UP:
        BusProvider.getInstance().post(new DoHUDViewMoveEvent(CONST.DIRECTION.UP));
        break;
      case DOWN:
        BusProvider.getInstance().post(new DoHUDViewMoveEvent(CONST.DIRECTION.DOWN));
        break;
      case LEFT:
        BusProvider.getInstance().post(new DoHUDViewMoveEvent(CONST.DIRECTION.LEFT));
        break;
      case RIGHT:
        BusProvider.getInstance().post(new DoHUDViewMoveEvent(CONST.DIRECTION.RIGHT));
        break;
      default:
        RuntimeException e = new RuntimeException("Unknown Joystick Action received at " + TAG + ": doJoystickAction");
        Crashlytics.logException(e);
        throw e;
    }
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.activity_main, menu);

    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    super.onOptionsItemSelected(item);

    handleMenuClick(item.getItemId());

    return false;
  }

  private void handleMenuClick(int menuItemId) {
    switch (menuItemId) {
      case R.id.menu_about:
        startActivity(new Intent(this, ActivityAbout.class));
        break;

      default:
        RuntimeException e = new RuntimeException(TAG + " handleMenuClick: Unknown menu item ID passed");
        Crashlytics.logException(e);
        throw e;
    }

  }

  /**
   * Labels in the activity are updated when new values for currents are available
   *
   * @param event
   */
  @Subscribe
  public void doUpdateCurrentsInActivity(UpdatedCurrentsAvailableEvent event) {

    // update the text view in the activity
    mTxtCurrents.setText("Maximum:\t" + event.currMax + "\nNow:\t" + event.currNow + "\nAverage:\t" + event.currAvg);
  }

  /**
   * HandlerNonLeaky instances request a new handler reference when their handler reference is stale
   *
   * @param event
   */
  @Subscribe
  public void getNewHandlerReference(GetNewHandlerReferenceEvent event) {
    // post a new handler reference on the bus
    BusProvider.getInstance().post(new NewHandlerReference(new Handler()));
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    //unregister from the bus
    BusProvider.getInstance().unregister(this);

    //stop the service if the phone is not connected
    if (!BatteryChangedReceiver.isChargerConnected(this))
      BusProvider.getInstance().post(new StopServiceEvent());
  }

  private class StopHUDBtnListener implements View.OnClickListener {
    /**
     * this flag isn't a reliable method to figure out if the service is running, but for the
     * purposes of the listener, works fine.
     */
    private boolean isServiceRunning = true;

    @Override
    public void onClick(View view) {
      if (isServiceRunning) {
        // send an event to stop the service
        BusProvider.getInstance().post(new StopServiceEvent());
        //change the button text to restart
        ((Button) view).setText(CONFIG.STOP_HUD_BTN_RESTART_TEXT);
        //set isServiceRunning to false since we just stopped it
        isServiceRunning = false;
      } else {
        //change the button text to restart
        ((Button) view).setText(CONFIG.STOP_HUD_BTN_STOP_TEXT);
        //start the service again
        startService(new Intent(getApplicationContext(), HUDService.class));
        //set isServiceRunning to true since we just started it
        isServiceRunning = true;
      }
    }
  }

  private class FeedbackBtnListener implements View.OnClickListener {
    @Override
    public void onClick(View view) {
      // show the feedback dialog
      feedbackDialog.show();

      //show that the feedback taking is in beta
      Toast.makeText(getApplicationContext(), CONFIG.FEEDBACK_IN_BETA, Toast.LENGTH_LONG).show();
    }
  }
}
