package com.osw.galaxy_charger_current_hud_overlay;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.osw.galaxy_charger_current_hud_overlay.events.StopServiceEvent;

/**
 * Created by Dr shobha on 14/8/13.
 */
public class BatteryChangedReceiver extends BroadcastReceiver {

  private static final String TAG = "BatteryChangedReceiver";

  @Override
  public void onReceive(Context context, Intent intent) {
    String action = intent.getAction();
    if (action.equals(Intent.ACTION_POWER_CONNECTED)) {
      Crashlytics.log(Log.INFO, TAG, "Charger Connected.");
      Intent startingIntent = new Intent(context, HUDService.class);
      context.startService(startingIntent);
    } else if (action.equals(Intent.ACTION_POWER_DISCONNECTED)) {
      Crashlytics.log(Log.INFO, TAG, "Charger Disconnected.");
      BusProvider.getInstance().post(new StopServiceEvent());
    }
  }

  /**
   * check if the phone is connected to either USB or AC
   *
   * @param context
   * @return true if phone is connected to either USB or AC
   * else false
   */
  public static boolean isChargerConnected(Context context) {
    //get a battery changed intent's current value
    Intent intent = context.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

    //get the plugged extras from intent
    int plugged = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);

    // phone is considered connected if its either plugged into USB or AC
    boolean isConnected = plugged == BatteryManager.BATTERY_PLUGGED_AC || plugged == BatteryManager.BATTERY_PLUGGED_USB;

    return isConnected;
  }
}
