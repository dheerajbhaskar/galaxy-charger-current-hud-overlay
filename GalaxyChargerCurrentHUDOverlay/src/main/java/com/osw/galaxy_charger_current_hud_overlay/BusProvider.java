package com.osw.galaxy_charger_current_hud_overlay;

/**
 * Created by Dr shobha on 29/7/13.
 */

import com.squareup.otto.Bus;

/**
 * Bus provider (singleton) for the otto event bus
 */
public class BusProvider {
  private static Bus busInstance = new Bus();

  public static Bus getInstance() {
    return busInstance;
  }
}
