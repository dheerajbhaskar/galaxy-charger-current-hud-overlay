package com.osw.galaxy_charger_current_hud_overlay;

/**
 * Created by Dr shobha on 1/8/13.
 */
public class CONFIG {

  public static final long JOYSTICK_KEY_REPEAT_DURATION = 50L;
  public static final String JOYSTICK_SAVE_MSG = "Saved";

  /**
   * in milliseconds
   */
  public static final long CURRENT_CONTINUOUS_MEASURE_INTERVAL = 1000L;
  public static final String ANDROID_FEEDBACK_API_KEY = "AF-01A36F6EB2EC-3C";
  public static final String FEEDBACK_IN_BETA = "Feedback is still in beta, there are open issues.";
  public static final String STOP_HUD_BTN_RESTART_TEXT = "Restart HUD";
  public static final String STOP_HUD_BTN_STOP_TEXT = "Stop HUD";

  /**
   * this is the interval between null checks when a startDebug method is called on a HandlerNonLeaky instance.
   * this controls how frequently null checks are displayed in logcat
   */
  public static final int NON_LEAKY_HANDLER_NULL_CHECK_INTERVAL = 500;

  /**
   * Created by Dr shobha on 6/8/13.
   */
  public static class PREFS {
    public static final float HUD_VIEW_FALLBACK_LOCATION_X = 100f;
    public static final float HUD_VIEW_FALLBACK_LOCATION_Y = 35f;
  }
}
