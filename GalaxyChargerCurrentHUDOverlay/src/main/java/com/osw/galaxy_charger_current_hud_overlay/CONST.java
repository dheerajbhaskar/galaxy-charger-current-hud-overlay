package com.osw.galaxy_charger_current_hud_overlay;

/**
 * Created by Dr shobha on 30/7/13.
 */
public class CONST {
  public static final String PREFERENCES_TAG = "com.osw.galaxy_charger_current_hud_overlay.preferences";

  /**
   * Various buttons on a joystick.
   * These are all the values a JoystickButtonPressedEvent class's button field will have.
   */
  public static enum JOYSTICK {
    UP, DOWN, LEFT, RIGHT
  }

  ;

  /**
   * common directions.
   * <p/>
   * currently used as directions a HUD View can move in
   */
  public static enum DIRECTION {
    UP, DOWN, LEFT, RIGHT
  }

  ;

  public static final class PREFS {
    public static final String VIEW_LOCATION_X = "VIEW_LOCATION_X";
    public static final String VIEW_LOCATION_Y = "VIEW_LOCATION_Y";
  }
}
