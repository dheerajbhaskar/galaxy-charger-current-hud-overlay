package com.osw.galaxy_charger_current_hud_overlay;

import android.os.Handler;
import android.util.Log;

import com.crashlytics.android.Crashlytics;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by Dr shobha on 2/8/13.
 */
public class Currents implements Runnable {
  private static final String TAG = "Currents";
  private String currMax = "N/A";
  private String currNow = "N/A";
  private String currAvg = "N/A";

  private HandlerNonLeaky mHandlerNonLeaky;
  private boolean isMeasuringContinuously = false;


  public Currents() {
    this.mHandlerNonLeaky = new HandlerNonLeaky(new Handler());
  }

  public String getCurrMax() {
    return currMax;
  }

  public String getCurrNow() {
    return currNow;
  }

  public String getCurrAvg() {
    return currAvg;
  }

  /**
   * updates the state variables currMax, currNow, and currAvg
   */
  public void measure() {
//        Log.d(TAG,"measure");

    // execute shell commands and get current values
    try {
      currMax = new BufferedReader(new InputStreamReader(Runtime.getRuntime().exec(new String[]{"cat", "/sys/class/power_supply/battery/current_now"}).getInputStream())).readLine();
      currNow = new BufferedReader(new InputStreamReader(Runtime.getRuntime().exec(new String[]{"cat", "/sys/class/power_supply/battery/current_avg"}).getInputStream())).readLine();
      currAvg = new BufferedReader(new InputStreamReader(Runtime.getRuntime().exec(new String[]{"cat", "/sys/class/power_supply/battery/current_max"}).getInputStream())).readLine();

      //post these new currents on to the event bus
      BusProvider.getInstance().post(new UpdatedCurrentsAvailableEvent(currMax, currNow, currAvg));
    } catch (Exception e) {
      Crashlytics.logException(e);
      e.printStackTrace();
    }
  }


  public boolean isMeasuringContinuously() {
    return isMeasuringContinuously;
  }

  public void measureContinuously() {
    Crashlytics.log(Log.VERBOSE, TAG, "measureContinuously");

    // set a flag so that we know this is running continuously
    isMeasuringContinuously = true;

    //remove this Runnable if present in the queue and post it
    mHandlerNonLeaky.removeCallbacksNonLeaky(this);
    mHandlerNonLeaky.postDelayedNonLeaky(this, 0L);
  }

  public void stopMeasuringContinuously() {
    Crashlytics.log(Log.VERBOSE, TAG, "STOP measureContinuously");

    // update the flag so that we know this STOPPED running continuously
    isMeasuringContinuously = false;

    //remove this Runnable if present in the queue
    mHandlerNonLeaky.removeCallbacksNonLeaky(this);
  }


  @Override
  public void run() {
//        Log.v(TAG, "run");

    //measure currents
    measure();

    //make this run again
    mHandlerNonLeaky.postDelayedNonLeaky(this, CONFIG.CURRENT_CONTINUOUS_MEASURE_INTERVAL);
  }
}
