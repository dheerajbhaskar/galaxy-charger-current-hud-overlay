package com.osw.galaxy_charger_current_hud_overlay;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.View;

import com.crashlytics.android.Crashlytics;
import com.osw.galaxy_charger_current_hud_overlay.events.DoHUDViewMoveEvent;
import com.osw.galaxy_charger_current_hud_overlay.events.DoUpdateHUDTextEvent;
import com.squareup.otto.Subscribe;

/**
 * Created by Dr shobha on 29/7/13.
 */
public class CustomView extends View {
  private static final String TAG = "CustomView";
  private final Paint paint;
  private final Context context;
  private String textOnHUD = "-- mA";

  // text's co-ords on Screen
  private float viewLocationX;
  private float viewLocationY;
  private SharedPreferences.Editor sharedPrefsEditor;


  public CustomView(Context context, float viewLocationX, float viewLocationY) {
    super(context);
    this.context = context;
    paint = getCustomPaint();
    sharedPrefsEditor = context.getSharedPreferences(CONST.PREFERENCES_TAG, Context.MODE_PRIVATE).edit();

    this.viewLocationX = viewLocationX;
    this.viewLocationY = viewLocationY;
  }

  @Override
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();

    //TODO the otto bus events viz. joystick events don't work on service restart, pls fix it

    //register on otto bus
    BusProvider.getInstance().register(this);
  }

  /**
   * get the kind of paint object you want
   *
   * @return
   */
  private Paint getCustomPaint() {
    Paint paint = new Paint();

    // customize paint object before returning
    paint.setTextSize(30);
    paint.setAntiAlias(true);
    paint.setTextAlign(Paint.Align.LEFT);
    paint.setSubpixelText(true);
    paint.setColor(Color.CYAN);

    return paint;
  }

  @Override
  protected void onDraw(Canvas canvas) {
    super.onDraw(canvas);

    // draw HUD text on canvas with the given paint
    canvas.drawText(textOnHUD, viewLocationX, viewLocationY, paint);
  }


  @Subscribe
  public void doUpdateHUDText(DoUpdateHUDTextEvent event) {
    Log.i(TAG, "doUpdateHUDText");
    textOnHUD = event.textToPutOnHUD;
    invalidate();
  }

  @Subscribe
  public void doHUDViewMove(DoHUDViewMoveEvent event) {
    Crashlytics.log(Log.VERBOSE, TAG, "doHUDViewMove " + event.direction);

    switch (event.direction) {
      case UP:
        viewLocationY -= 1;
        break;
      case DOWN:
        viewLocationY += 1;
        break;
      case LEFT:
        viewLocationX -= 1;
        break;
      case RIGHT:
        viewLocationX += 1;
        break;
    }

    //save the new location in prefs

    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.GINGERBREAD) {
      // only for gingerbread and newer versions
      //apply is available from API Level 9 onwards
      sharedPrefsEditor.putFloat(CONST.PREFS.VIEW_LOCATION_X, viewLocationX);
      sharedPrefsEditor.putFloat(CONST.PREFS.VIEW_LOCATION_Y, viewLocationY).apply();
    } else {
      sharedPrefsEditor.putFloat(CONST.PREFS.VIEW_LOCATION_X, viewLocationX);
      sharedPrefsEditor.putFloat(CONST.PREFS.VIEW_LOCATION_Y, viewLocationY).commit();
    }


    //re-draw the view
    invalidate();
  }

  @Subscribe
  public void doUpdateCurrentsInHUD(UpdatedCurrentsAvailableEvent event) {
    Crashlytics.log(Log.VERBOSE, TAG, "doUpdateCurrentsInHUD");
    String min = findMinNoConvert(event.currAvg, event.currMax, event.currNow);
    textOnHUD = min + " mA";
    invalidate();
  }

  /**
   * method to find the minimum of 3 currents without doing conversions
   *
   * @param currAvg
   * @param currMax
   * @param currNow
   * @return min of currAvg, currMax and currNow
   */
  private String findMinNoConvert(String currAvg, String currMax, String currNow) {
    //if currAvg is the min
    if (currAvg.compareTo(currMax) <= 0) {
      if (currAvg.compareTo(currNow) <= 0) {
        return currAvg;
      }
    }

    //if currMax is the min
    if (currMax.compareTo(currAvg) <= 0) {
      if (currMax.compareTo(currNow) <= 0) {
        return currMax;
      }
    }

    //if currNow is the min
    if (currNow.compareTo(currMax) <= 0) {
      if (currNow.compareTo(currAvg) <= 0) {
        return currNow;
      }
    }

    return null;
  }

  @Override
  protected void onDetachedFromWindow() {
    super.onDetachedFromWindow();

    //unregister on otto bus
    BusProvider.getInstance().unregister(this);
  }

  public float getX() {
    return viewLocationX;
  }

  public float getY() {
    return viewLocationY;
  }
}
