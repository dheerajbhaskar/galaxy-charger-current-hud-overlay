package com.osw.galaxy_charger_current_hud_overlay;


import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;

import com.crashlytics.android.Crashlytics;
import com.osw.galaxy_charger_current_hud_overlay.events.StopServiceEvent;
import com.squareup.otto.Subscribe;

public class HUDService extends Service {

  private CustomView viewHUD;

  public static final String TAG = "HUDService";
  private SharedPreferences sharedPrefs;
  private Currents mCurrents;

  /**
   * onCreate is run only once, i.e. when a service is not running
   */
  @Override
  public void onCreate() {
    super.onCreate();
    Crashlytics.log(Log.DEBUG, TAG, "onCreate");

    //register on otto bus
    BusProvider.getInstance().register(this);

    //get SharedPreferences and its editor
    sharedPrefs = getSharedPreferences(CONST.PREFERENCES_TAG, MODE_PRIVATE);

    //init current vars
    mCurrents = new Currents();
    mCurrents.measureContinuously();
    String currMax = mCurrents.getCurrMax();
    String currNow = mCurrents.getCurrNow();
    String currAvg = mCurrents.getCurrAvg();

    //get location of HUD view from SharedPreferences
    float hudXLocationFromPrefs = sharedPrefs.getFloat(CONST.PREFS.VIEW_LOCATION_X, getScreenWidth() / 2);
    float hudYLocationFromPrefs = sharedPrefs.getFloat(CONST.PREFS.VIEW_LOCATION_Y, CONFIG.PREFS.HUD_VIEW_FALLBACK_LOCATION_Y);

    // add the view to System Overlay
    viewHUD = new CustomView(getApplicationContext(), hudXLocationFromPrefs, hudYLocationFromPrefs);
    addToHUD(viewHUD);

  }

  /**
   * @return the width of the screen in px
   */
  private int getScreenWidth() {
    WindowManager wm = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
    Display display = wm.getDefaultDisplay();

    int width;
    int height;
    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
      //getSize was introduced in API Level 13
      Point size = new Point();
      display.getSize(size);
      width = size.x;
      height = size.y;
    } else {
      width = display.getWidth();
      height = display.getHeight();
    }

    return width;

  }

  /**
   * This is called every time startService(Intent) is executed, but onCreate() is called if the service is not running yet
   *
   * @param intent
   * @param flags
   * @param startId
   * @return
   */
  @Override
  public int onStartCommand(Intent intent, int flags, int startId) {
    super.onStartCommand(intent, flags, startId);
    Log.d(TAG, "onStartCommand");

    // returning start sticky since we want the service to be restarted if it is stopped by the OS for some reason
    return START_STICKY;
  }

  /**
   * add the passed view to System Overlay
   *
   * @param viewToAdd
   */
  private void addToHUD(View viewToAdd) {
    Crashlytics.log(Log.DEBUG, TAG, "addToHUD");
    // make the background of the view transaprent so that other screen elements are visible through it
    viewToAdd.setBackgroundColor(Color.TRANSPARENT);

    // make the required params object for the HUD viewToAdd
    WindowManager.LayoutParams params = new WindowManager.LayoutParams(
        WindowManager.LayoutParams.WRAP_CONTENT,
        WindowManager.LayoutParams.WRAP_CONTENT,
        WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY,
        WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN | WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
        PixelFormat.TRANSPARENT);
    params.gravity = Gravity.RIGHT | Gravity.TOP;
    WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);

    //add the given view to the WindowManager with given params
    wm.addView(viewToAdd, params);
  }

  /**
   * a method which enables to stop the service from any other android component
   *
   * @param event
   */
  @Subscribe
  public void stopHUDService(StopServiceEvent event) {
    stopSelf();
  }


  @Override
  public void onDestroy() {
    super.onDestroy();
    Crashlytics.log(Log.DEBUG, TAG, "onDestroy");
    if (viewHUD != null) {
      ((WindowManager) getSystemService(WINDOW_SERVICE)).removeView(viewHUD);
      viewHUD = null;
    }

    //unregister from the bus
    BusProvider.getInstance().unregister(this);

    //stop continuous measurement if running
    if (mCurrents.isMeasuringContinuously())
      mCurrents.stopMeasuringContinuously();
  }

  @Override
  public IBinder onBind(Intent intent) {
    Crashlytics.log(Log.DEBUG, TAG, "onBind");
    return null;
  }

}
