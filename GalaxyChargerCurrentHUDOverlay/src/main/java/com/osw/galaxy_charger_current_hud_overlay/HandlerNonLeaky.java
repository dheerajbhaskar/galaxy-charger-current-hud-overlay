package com.osw.galaxy_charger_current_hud_overlay;

import android.os.Handler;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.osw.galaxy_charger_current_hud_overlay.events.GetNewHandlerReferenceEvent;
import com.osw.galaxy_charger_current_hud_overlay.events.NewHandlerReference;
import com.squareup.otto.Subscribe;

import java.lang.ref.WeakReference;

/**
 * Created by Dr shobha on 2/8/13.
 */

/**
 * provides a handler that doesn't leak the Context, since it only holds a weak reference to the Context.
 * This class thows a RuntimeException if the handler object is Garbage Collected and getHandler is called.
 * <p/>
 * This is done because some life cycle or design issue needs to be re-thought if a handler is needed after GC.
 */
public class HandlerNonLeaky {
  private static final String TAG = "HandlerNonLeaky";
  private WeakReference<Handler> handlerWeakReference;
  private Runnable nullCheckDebugTask;

  public HandlerNonLeaky(Handler handler) {
    this.handlerWeakReference = new WeakReference<Handler>(handler);
  }

  /**
   * continuously prints to logcat if the handler is null or not
   */
  public void startDebug() {
    //save to a field variable so that we can remove this callback from queue when needed
    nullCheckDebugTask = new NullCheckDebugTask();

    //add this to the run queue
    postDelayedNonLeaky(nullCheckDebugTask, CONFIG.NON_LEAKY_HANDLER_NULL_CHECK_INTERVAL);
  }

  /**
   * provides no guarantee that the runnable will be processed. Neither do the native APIs
   *
   * @param runnable
   * @param delayedBy
   */
  public void postDelayedNonLeaky(Runnable runnable, long delayedBy) {
    if (handlerWeakReference.get() instanceof Handler)
      handlerWeakReference.get().postDelayed(runnable, delayedBy);
    else {
      Crashlytics.log(Log.VERBOSE, TAG, "postDelayedNonLeaky: Runnable DROPPED! i.e. NOT processed.");

      //request a new handler reference from an *alive* activity
      requestHandlerUpdate();
    }
  }

  public void removeCallbacksNonLeaky(Runnable runnable) {
    if (handlerWeakReference.get() instanceof Handler)
      handlerWeakReference.get().removeCallbacks(runnable);
    else {
      Crashlytics.log(Log.VERBOSE, TAG, "removeCallbacksNonLeaky: NOT processed.");

      //request a new handler reference from an *alive* activity
      requestHandlerUpdate();
    }
  }

  /**
   * convenience method which posts GetNewHandlerReferenceEvent on otto to request an updated handler from an ALIVE activity
   * NOTE: only this instance of HandlerNonLeaky gets an updated reference since only *this* registers on to the bus (and unregisters later)
   */
  private void requestHandlerUpdate() {
    //register on the otto bus so that we can receive a new handler
    BusProvider.getInstance().register(this);
    Crashlytics.log(Log.VERBOSE, TAG, "requestHandlerUpdate: Registered on Otto");

    //request a new handler on the bus
    BusProvider.getInstance().post(new GetNewHandlerReferenceEvent());
    Crashlytics.log(Log.VERBOSE, TAG, "requestHandlerUpdate: Requested new reference");
  }

  /**
   * This is where the new handler is received when requested from requestHandlerUpdate() method
   *
   * @param event
   */
  @Subscribe
  public void updateHandlerReference(NewHandlerReference event) {
    //get the updated handler reference from the event object and update it in *this* HandlerNonLeaky object
    handlerWeakReference = new WeakReference<Handler>(event.newHandler);
    Crashlytics.log(Log.VERBOSE, TAG, "updateHandlerReference: Handler *Updated*");

    //unregister from the otto bus since our work is done
    BusProvider.getInstance().unregister(this);
    Crashlytics.log(Log.VERBOSE, TAG, "updateHandlerReference: Unregistered from the Otto bus");
  }


  /**
   * A runnable task which checks if the handler is null.
   * <p/>
   * DIDN"T QUITE WORK AS IT WAS SUPPOSED TO. HANDLER NEVER BECAME NULL, PERHAPS DUE TO THE NullCheckDebugTask ITSELF.
   */
  private class NullCheckDebugTask implements Runnable {
    private static final String DEBUG_TAG = "NullCheckDebugTask";

    @Override
    public void run() {
      boolean isHandlerNull = handlerWeakReference.get() == null;

      Crashlytics.log(Log.VERBOSE, DEBUG_TAG, "isNull " + isHandlerNull);

      postDelayedNonLeaky(this, CONFIG.NON_LEAKY_HANDLER_NULL_CHECK_INTERVAL);
    }
  }
}
