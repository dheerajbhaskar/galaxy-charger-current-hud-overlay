package com.osw.galaxy_charger_current_hud_overlay;

import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.crashlytics.android.Crashlytics;
import com.osw.galaxy_charger_current_hud_overlay.events.JoystickButtonPressedEvent;

/**
 * Created by Dr shobha on 30/7/13.
 */

/**
 * An instance of this class must be used as a TouchListener
 * <p/>
 * This class just provides the wiring i.e. posts a JoystickButtonPressedEvent for each kind of
 * CONST.JOYSTICK event that occurs. The logic/implementation must be handled by a method that subscribes to
 * this event
 */
public class JoystickRepeatListener implements View.OnTouchListener, Runnable {
  public static final String TAG = "JoystickRepeatListener";
  private final HandlerNonLeaky mHandlerNonLeaky;
  private long downDuration;
  private CONST.JOYSTICK currentBtn = null;

  public JoystickRepeatListener() {
    mHandlerNonLeaky = new HandlerNonLeaky(new Handler());
  }

  /**
   * See the class description. This method just provides wiring and throws events
   * For following params and return values see android View.OnTouchListener
   *
   * @param view
   * @param motionEvent
   * @return
   */
  @Override
  public boolean onTouch(View view, MotionEvent motionEvent) {
    if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {

      downDuration = 0;
      //send ID and ask which Button it is
      currentBtn = whichBtn(view.getId());
      Crashlytics.log(Log.INFO, TAG, "onTouch: ACTION_DOWN " + currentBtn + " pressed");

      //remove this call back if exists then post a new one
      mHandlerNonLeaky.removeCallbacksNonLeaky(this);
      mHandlerNonLeaky.postDelayedNonLeaky(this, CONFIG.JOYSTICK_KEY_REPEAT_DURATION);
    } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
      Crashlytics.log(Log.INFO, TAG, "onTouch: ACTION_UP " + currentBtn + " released" + " downDuration=" + downDuration + " ms");

      //make the currentBtn null so that we can't use it again by mistake
      currentBtn = null;
      //remove the repeating call back
      mHandlerNonLeaky.removeCallbacksNonLeaky(this);
    }

    return false;
  }

  /**
   * a convenience method which takes in the View's ID and return which button of Joystick was pressed.
   * IDs of buttons MUST be:
   * <BUTTON:ID>
   * UP:btnUp
   * DOWN:btnDown
   * LEFT:btnLeft
   * RIGHT:btnRight
   *
   * @param viewID
   * @return
   */
  private CONST.JOYSTICK whichBtn(int viewID) {
    switch (viewID) {
      case R.id.btnUp:
        return CONST.JOYSTICK.UP;
      case R.id.btnDown:
        return CONST.JOYSTICK.DOWN;
      case R.id.btnRight:
        return CONST.JOYSTICK.RIGHT;
      case R.id.btnLeft:
        return CONST.JOYSTICK.LEFT;
      default:
        RuntimeException e = new RuntimeException("Invalid View ID relieved at " + TAG);
        Crashlytics.logException(e);
        throw e;
    }
  }

  /**
   * Used here to get the repeating-on-pressed-down behavior
   */
  @Override
  public void run() {
    downDuration += CONFIG.JOYSTICK_KEY_REPEAT_DURATION;
    Crashlytics.log(Log.INFO, TAG, "run: down duration" + downDuration + "ms");

    // if the current button is not null (i.e. instance of JOYSTICK) then post the event
    if (currentBtn instanceof CONST.JOYSTICK)
      BusProvider.getInstance().post(new JoystickButtonPressedEvent(currentBtn));

    //call this runnable again
    mHandlerNonLeaky.postDelayedNonLeaky(this, CONFIG.JOYSTICK_KEY_REPEAT_DURATION);

  }
}
