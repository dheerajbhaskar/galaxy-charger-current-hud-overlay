package com.osw.galaxy_charger_current_hud_overlay;

/**
 * Created by Dr shobha on 2/8/13.
 */

/**
 * This event is fired when a measure happens
 */
public class UpdatedCurrentsAvailableEvent {
  public String currMax;
  public String currNow;
  public String currAvg;

  public UpdatedCurrentsAvailableEvent(String currMax, String currNow, String currAvg) {
    this.currMax = currMax;
    this.currNow = currNow;
    this.currAvg = currAvg;
  }
}
