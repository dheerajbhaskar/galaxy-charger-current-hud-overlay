package com.osw.galaxy_charger_current_hud_overlay.events;

/**
 * Created by Dr shobha on 29/7/13.
 */

import com.osw.galaxy_charger_current_hud_overlay.CONST;

/**
 * Event which will be fired when we want a HUD View to move;
 * The view can decide how it wants to implement this moving
 */
public class DoHUDViewMoveEvent {
  public CONST.DIRECTION direction;

  public DoHUDViewMoveEvent(CONST.DIRECTION direction) {
    this.direction = direction;
  }
}
