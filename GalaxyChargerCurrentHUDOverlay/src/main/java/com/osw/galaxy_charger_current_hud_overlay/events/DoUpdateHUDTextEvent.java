package com.osw.galaxy_charger_current_hud_overlay.events;

/**
 * Created by Dr shobha on 29/7/13.
 */

/**
 * Event which will be fired when an update to the HUD Text is needed
 */
public class DoUpdateHUDTextEvent {

  public String textToPutOnHUD;

  public DoUpdateHUDTextEvent(String textToPutOnHUD) {
    this.textToPutOnHUD = textToPutOnHUD;
  }
}
