package com.osw.galaxy_charger_current_hud_overlay.events;

import com.osw.galaxy_charger_current_hud_overlay.CONST;

/**
 * Created by Dr shobha on 30/7/13.
 */
public class JoystickButtonPressedEvent {

  /**
   * Enum containing which button is pushed
   */
  public static CONST.JOYSTICK button;

  public JoystickButtonPressedEvent(CONST.JOYSTICK button) {
    this.button = button;
  }
}
