package com.osw.galaxy_charger_current_hud_overlay.events;

import android.os.Handler;

/**
 * Created by Dr shobha on 14/8/13.
 */
public class NewHandlerReference {
  public Handler newHandler;

  public NewHandlerReference(Handler newHandler) {
    this.newHandler = newHandler;
  }
}
